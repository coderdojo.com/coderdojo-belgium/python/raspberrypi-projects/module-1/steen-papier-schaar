# 02. Steen Papier Schaar

https://projects.raspberrypi.org/nl-NL/projects/rock-paper-scissors

- [Speler's beurt](https://projects.raspberrypi.org/nl-NL/projects/rock-paper-scissors/1)
- [Computer's beurt](https://projects.raspberrypi.org/nl-NL/projects/rock-paper-scissors/2)
- [Controller het resultaat](https://projects.raspberrypi.org/nl-NL/projects/rock-paper-scissors/3)