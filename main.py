#!/bin/python3

from random import randint

# Als je de vraag wil laten stellen, verwijder dan de '#' voor de volgende lijn
# speler = input('steen (t), papier (p) of schaar (s)')
# En zet een '#' voor volgende lijn
speler = 't'

print(speler, 'vs', end=' ')

gekozen = randint(1,3)
# print (gekozen)

if gekozen == 1: 
    computer = 't'
elif gekozen == 2:
    computer = 'p'
else:
    computer = 's'

print(computer)

if speler == computer:
    print('Gelijk!')

elif speler == 't' and computer == 's':
    print('Speler wint!')

elif speler == 't' and computer == 'p':
    print('Computer wint!')

elif speler == 'p' and computer == 't':
    print('Speler wint!')

elif speler == 'p' and computer == 's':
    print('Computer wint!')
    